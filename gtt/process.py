"""Functions to run something in process, without showing window, and communicating back progress with signals"""


import subprocess
import os
import time
import threading


def background_process(cmd: list, env=None, log=None, heartbeat=False):
    """Runs a subprocess.Popen in the background, with hidden shell"""

    # Hide the window
    startupinfo = None
    if os.name == "nt":
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW

    # Add environment if none is given
    if env is None:
        env = os.environ

    try:
        process = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stdin=subprocess.DEVNULL,
            stderr=subprocess.STDOUT,
            env=env,
            startupinfo=startupinfo,
            universal_newlines=True,
        )
    except OSError as e:
        print("OSError exception: ", e)
    return communicate_process(process, log, heartbeat)


def communicate_process(process: subprocess.Popen, log: callable, heartbeat=False):
    """Handles non-blocking subprocess communication and return each line of output
    
    Args:
        process (Popen): the Popen'ed subprocess
        log (callable): Callback function for logging
    """

    if heartbeat and log is not None:

        def heartbeat_tick(log):
            last_ping = time.time()
            while heartbeat:
                if time.time() - last_ping > 60:
                    last_ping = time.time()
                    log(f"Processing ... {time.strftime('%X')}")
                    time.sleep(20)

        heartbeat_thread = threading.Thread(target=heartbeat_tick, args=[log])
        heartbeat_thread.start()
    else:
        heartbeat_thread = None

    while True:
        line = process.stdout.readline()  # type: str
        if not line and process.poll() is not None:
            break
        if line:
            if log is not None:
                log(line.strip())

    return_code = process.poll()
    process.kill()

    if heartbeat_thread is not None:
        heartbeat = False
        heartbeat_thread.join()

    return return_code
