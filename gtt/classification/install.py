"""
    Handles the installation of the OrfeoToolbox
"""

import tempfile
import platform
from pathlib import Path
from functools import partial

from qgis.utils import iface
from qgis.core import Qgis
from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QFileDialog

from gtt.downloader import Download
from gtt.functions import show_progress, get_temp_path, message_log


version = "OTB-7.3.0"
system = platform.system().lower()
if system == "windows":
    defaultdir = Path("C:\\")
    url = f"https://www.orfeo-toolbox.org/packages/{version}-Win64.zip"
elif system == "linux":
    defaultdir = Path("opt")
    url = f"https://www.orfeo-toolbox.org/packages/{version}-Linux64.run"
elif system == "darwin":
    defaultdir = Path("bin")
    url = f"https://www.orfeo-toolbox.org/packages/{version}-Darwin64.run"

Url = Path(url)


def prepare(force=False):
    """
        Ask install location, downloads and unpacks. Async, so install isn't done yet when this function ends!
    """

    if not force:
        if check_installed():
            iface.messageBar().pushSuccess("OTB", f"OTB is already properly installed")
            return True

    # Ask for install location if it's not set
    returndir = QFileDialog.getExistingDirectory(
        iface.mainWindow(), "Select OrfeoToolbox install location", str(defaultdir)
    )
    if returndir is "":
        return False

    # If we selected a folder that has OTB in it, save that
    otb_path = Path(returndir).resolve()
    file = otb_path / "bin" / "otbApplicationLauncherCommandLine.exe"
    if file.exists():        
        iface.messageBar().pushSuccess("OTB", f"Using existing install location")
        iface.gtt.setting_set("otb/location", str(otb_path))
        return True

    otb_path = otb_path / version  # Add the subfolder to extract it into
    iface.gtt.setting_set("otb/location", str(otb_path))

    install(otb_path)


def check_installed():
    """Checks if otb is installed correctly"""
    location = iface.gtt.settings["otb/location"]
    if location in ("", "None"):
        return False
    else:
        program = Path(location) / "bin" / "otbApplicationLauncherCommandLine.exe"  # type: Path
        return program.exists()


def install(path: Path, showprogress=True):
    """
        Downloads OrfeoToolbox to temp location and unpacks it, will show progress
    """
    if system in ("linux", "darwin"):
        raise NotImplementedError(
            f"{system} installation is currently not supported, please install it yourself and set the location in QGIS Settings"
        )


    unzip_path: Path = path.parent
    unzip_path.mkdir(0x757, parents=True, exist_ok=True)
    name = Path(url).name

    target_file = get_temp_path() / name
    target_file.parent.mkdir(parents=True, exist_ok=True)

    download = Download()
    download.get_file(url, target_file, unzip_path, cleanup=True)

    # Handle cleanup and progress reporting
    progressbar = None
    if showprogress:
        progressbar = show_progress("Downloading OrfeoToolbox", range_=(0, 100))

        def _progress(progressbar, progress):
            progressbar.setProgress(progress)

        download.progress.connect(partial(_progress, progressbar))

    def _install_done(progressbar, download, path):
        if progressbar is not None:
            progressbar.setDuration(5)
            progressbar.setText("Succesfully downloaded OrfeoToolbox")
            message_log("<br><b>Download done</b>")
        download.deleteLater()

    def _error(progressbar, download, title, text):
        if progressbar is not None:
            progressbar.setDuration(5)
            progressbar.setText("Failed OrfeoToolbox download")

        iface.messageBar().pushCritical(title, text)
        text = (
            f"<br/><b>{text} - Read this</b><br/>"
            "This usually means the download URL has changed. You can manually install"
            " by downloading the latest OTB zip, extracting it and selecting that folder when asked for Install Location."
        )

        message_log(text, level=Qgis.Critical)
        download.deleteLater()

    download.done.connect(partial(_install_done, progressbar, download))
    download.error.connect(partial(_error, progressbar, download))
