"""
from gtt.classification import test
test.test_thread()
"""

import subprocess
import threading
from pathlib import Path

from qgis.core import QgsRasterLayer, QgsVectorLayer, QgsProject, QgsApplication
from qgis.utils import iface

from gtt.extent import extent_utm_zone

from . import get_file_dimensions
from .roi import functions, surfaces
from .roi.symbol import create_banded_colors
from . import Classification, ThreadReturn, tiff_convert, add_classified_file


def step1(path: str) -> QgsRasterLayer:
    crs = extent_utm_zone()
    tif = tiff_convert(Path(path), crs=crs)
    if tif is not None:
        layer = QgsRasterLayer(str(tif), "gtt_satmap2")
        QgsProject.instance().addMapLayer(layer)
        return layer
    else:
        return None


def step2(layer: QgsVectorLayer):
    classifier = Classification(parent=iface.gtt)
    task = classifier.main(Path(layer.source()))
    return task


def step3(task: ThreadReturn, path: str) -> QgsRasterLayer:
    layer = QgsRasterLayer(path, "gtt_mask")
    QgsProject.instance().addMapLayer(layer)
    classes = surfaces.load_surfaces()
    create_banded_colors(layer, classes)
    return layer


def full_test(path: str):
    layer = step1(path)
    task = step2(layer)
    task.done.connect(step3)


def test_thread():
    def _thread(obj):
        path = r"B:\Arma\Frontline\QGIS_ArmaTerrainTools\uwot\gtt_export\gtt_satmap_out_out.tif"
        obj.done.emit(obj, path)

    obj = ThreadReturn(parent=QgsApplication.instance())
    obj.done.connect(add_classified_file)
    thread = threading.Thread(target=_thread, args=[obj])
    thread.start()
