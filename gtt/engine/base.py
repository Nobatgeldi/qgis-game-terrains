import subprocess
import re

from gtt.functions import qgis_bin_folder, message_log

def power_of_two(num: int):
    """Checks if given number is a power of two
    
    Args:
        num (int)
    """
    return ((num & (num - 1)) == 0) and num > 0


def set_tooltip(widget, text: str):
    """Sets tooltip and and saves the original one, to restore for later usage
    
    Args:
        widget ([type]): Qt Widget to setToolTip to
        text (str): Text, use empty string to reset
    """
    if text != "":
        if not hasattr(widget, "_tooltip"):
            widget._tooltip = widget.toolTip()
        widget.setToolTip(text)
    else:
        if hasattr(widget, "_tooltip"):
            widget.setToolTip(widget._tooltip)
        else:
            widget.setToolTip("")


class Engine:
    COORD_X = 0
    COORD_Y = 0
    CRS = "EPSG:32631"
    NAME = "Default engine"
    SATMAP_FORMAT = ".bmp"  # File format for Printer
    MASK_SUPPORT = True  # Sets to False to prevent mask export
    TILED_EXPORT = False  # Tiled satmap export allowed

    def __str__(self):
        return self.NAME

    def heightResolutionValidate(self, *args, **kwargs):
        raise NotImplementedError

    def heightExport(self, *args, **kwargs):
        raise NotImplementedError

    def exportLog(self, file):
        """Called at the end of export, writes to gtt_export.txt file"""
        pass


def export_heightmap_info(filepath): 
    """Print min/max height data required to be set in Enfusion (Needs to entered manually)"""

    gdal = qgis_bin_folder() / "gdalinfo.exe"
    cmd = [str(gdal), "-stats", str(filepath)]

    message_log(f"Detecting height data")
    pipe = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = pipe.communicate()

    min_height = re.findall(r"(?<=STATISTICS_MINIMUM=)\-?\d{1,}", stdout.decode("utf8"))[0]
    max_height = re.findall(r"(?<=STATISTICS_MAXIMUM=)\-?\d{1,}", stdout.decode("utf8"))[0]
    message_log(f"<b>Minimum height: {min_height}m</b>")
    message_log(f"<b>Maximum height: {max_height}m</b>")
    message_log(f"<b>height range: {int(max_height) - int(min_height)}m</b>")
    return min_height, max_height
