from typing import Tuple
from PyQt5.QtCore import QObject, QVariant, pyqtSignal, pyqtSlot

from qgis.core import QgsTask, Qgis, QgsVectorLayer, QgsFeature
from gtt.functions import apply_stylefile, message_log, attribute_column


def process_shapes(sourcelayer):
    """Process the osm multipolygons"""


class ProcessShapes(QgsTask):
    layer: QgsVectorLayer

    def __init__(self, layer):
        super().__init__(description="Processing shapes")
        self.layer = layer
        self.length = layer.featureCount()
        self._completed = True
        self._categories = {}

    def run(self):
        message_log(f"Shapes processing start")

        self.layer.setSubsetString("")
        self.layer.startEditing()

        shape_column = attribute_column(self.layer, "_TYPE", QVariant.String)
        sub_column = attribute_column(self.layer, "_SUBTYPE", QVariant.String)

        i_ = 0
        for feature in self.layer.getFeatures():
            if self.isCanceled():
                self._completed = False
                break

            shape_type, sub_type = self.categorize_shape(feature)
            self.layer.changeAttributeValue(feature.id(), shape_column, shape_type)
            self.layer.changeAttributeValue(feature.id(), sub_column, sub_type)
            self.setProgress(i_ / self.length * 100)
            i_ += 1

        self.layer.commitChanges()
        apply_stylefile(self.layer, "mask_export")
        self._completed = True

        if self._completed:
            message_log("Shapes processing completed")
        else:
            message_log("Shapes processing failed", level=Qgis.Critical)

        return self._completed

    def categorize_shape(self, feature: QgsFeature) -> Tuple[str, str]:
        """
            Creates simplified classification, reduces from like 50 types to about 10
        """
        landuse = feature["landuse"]
        leisure = feature ["leisure"]
        place = feature["place"]
        natural = feature["natural"]
        other_tags = feature["other_tags"]
        amenity = feature["amenity"]
        aeroway = feature["aeroway"]

        # Farm grass
        if landuse in ('farmland', 'animal_keeping', 'orchard', 'Farm_land'):
            return ("farm_grass", landuse)
        if place in ('farm',):
            return ("farm_grass", place)
        elif landuse in ("plant_nursery",):
            return ("farm_grass", landuse)
        # Residental grass
        elif landuse in ('farmyard', 'residential', 'allotments', 'vineyard', 'greenfield', 'greenhouse_horticulture', 'retail'):
            return ("residential_grass", landuse)
        elif leisure in ('playground',):
            return ("residential_grass", leisure)
        # Clean grass
        elif leisure in ('pitch', 'stadium', 'recreation_ground'):
            return ("clean_grass", leisure)
        elif landuse in ('cemetery',):
            return ("clean_grass", landuse)
        elif natural in ('grassland',):
            return ("clean_grass", natural)
        # Grass
        elif natural in ("grass",):
            return ("grass", natural)
        # Trees
        elif landuse in ("forest", "meadow"):
            return ("trees", landuse)
        elif natural in ("wood",):
            return ("trees", natural)
        elif natural in ("heath",):
            return ("heath", natural)
        # Concrete
        elif landuse in ('industrial', 'depot', 'port'):
            return ('industrial', landuse)
        elif amenity in ('parking',):
            return ("industrial", amenity)
        elif aeroway in ('aerodrome', 'helipad'):
            return ("industrial", aeroway)
        # Dirt
        elif landuse in ('construction', 'landfill', 'quarry', 'brownfield'):
            return ('dirt', landuse)
        elif landuse in ('salt_pond',):
            return ('flats', landuse)
        # Water
        elif natural in ('water', 'wetland'):
            return ('water', natural)
        elif leisure in ('marina',):
            return ('water', leisure)
        elif landuse in ('reservoir', 'basin', 'aquaculture', 'pond', 'fishfarm'):
            return ('water', landuse)
        elif natural in ('beach', 'harbour'):
            return ('beach', landuse)
        elif natural in ('coastline', 'bare_rock'):
            return ('rocks', natural)

        return ('unknown', 'unknown')