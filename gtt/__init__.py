from qgis.utils import iface
from gtt.install import install_requirements

if __name__ == "gameterraintools":
    install_requirements()

from gtt.plugin import GameTerrainTools


def classFactory(iface):
    """Load GameTerrainTools class from file GameTerrainTools.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    return GameTerrainTools(iface)
