

Make MR :3


This is mainly personal reminder on how to dev this.

## Debugging

* Run `OSGeo4W.bat` from the QGIS install
* Run `python -m pip install ptvsd --user -- force`
* Restart QGIS
* Use Plugins > Game Terrain Tools > Debug Attaching
* Run the `Python: Remote Attach` debug task in VSCode
