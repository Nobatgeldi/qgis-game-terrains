import requests
import os


def create_session():
    api_key = os.getenv("GITLAB_API_KEY", None)
    session = requests.session()
    session.headers.update({"PRIVATE-TOKEN": api_key})
    return session


def create_page():
    project_id = os.getenv("CI_PROJECT_ID", None)
    url_base = os.getenv("GITLAB_API_URL", "https://gitlab.com/api/v4/")
    url_wiki = f"{url_base}projects/{project_id}/wikis"
    data = {
        "title": "Test",
        "slug": "test",
        "format": "markdown",
        "content": "Test Page",
    }
    session = create_session()
    session.post(url_wiki, data=data)


def page_exists(session, url) -> bool:
    response = session.get(url)
    return response.status_code == 200


def update_page(title: str, content: str, format="markdown"):
    project_id = os.getenv("CI_PROJECT_ID", None)
    url_base = os.getenv("GITLAB_API_URL", "https://gitlab.com/api/v4/")
    slug = title.lower()
    url_wiki = f"{url_base}projects/{project_id}/wikis/{slug}"

    session = create_session()
    if not page_exists(session, url_wiki):
        data = {
            "title": title,
            "id": slug,
            "format": "markdown",
            "content": content,
        }
        url_wiki = f"{url_base}projects/{project_id}/wikis"
        response = session.post(url_wiki, data=data)
    else:
        data = {
            "title": title,
            "slug": slug,
            "format": "markdown",
            "content": content,
        }
        response = session.put(url_wiki, data=data)
    return response


def update_home(content: str):
    response = update_page("home", content)
