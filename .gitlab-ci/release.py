import sys
import subprocess
from functions import release_tags


def bump_version(minor=True) -> str:
    tags = release_tags()
    if tags is None:
        last = ["0.0.0"]

    last = tags[-1]
    if last.startswith("v"):
        has_v = True
        last = last[1:]
    else:
        has_v = False

    parts = last.split(".")
    if len(parts) < 2:
        parts.append("0")
    if len(parts) < 3:
        parts.append("0")

    index = [1, 2][minor]
    current = int(parts[index])
    current += 1
    parts[index] = str(current)

    version = ".".join(parts)
    version = "v" + version
    return version


if __name__ == "__main__":
    tag = sys.argv[1]
    if tag == "minor":
        tag = bump_version(True)
    elif tag == "major":
        tag = bump_version(False)

    tag = f"release/{tag}"
    subprocess.call(["git", "tag", "-a", tag, "-m", f"Release {tag}"], shell=True)
    subprocess.call(["git", "push", "--follow-tags"], shell=True)
